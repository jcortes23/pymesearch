﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Master.Master" AutoEventWireup="true" CodeBehind="AdministrarComercio.aspx.cs" Inherits="PymeSearch.Views.AdministrarComercio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="https://unpkg.com/leaflet@1.0.2/dist/leaflet.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.2/dist/leaflet.css" />
    <script src="../Assets/Js/AdministrarComercio.js"></script>
    <div class="container">
        <div class="row">
            <h4>Administrar comercio</h4>
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Nombre:</label>
                    <input type="text" class="form-control">
                </div>

                <div class="form-group">
                    <label>Correo:</label>
                    <input type="email" class="form-control">
                </div>

                <div class="form-group">
                    <label>Tipo de comercio:</label>
                    <select class="form-control">
                        <option>Seleccione...</option>
                        <option>Panaderia</option>
                        <option>Peluqueria</option>
                        <option>Barberia</option>
                        <option>Soda</option>
                    </select>
                </div>
            </div>

            <div class="col">
                <div class="form-group">
                    <label>Telefono:</label>
                    <input type="tel" class="form-control">
                </div>

                <div class="form-group">
                    <label>Contraseña:</label>
                    <input type="password" class="form-control">
                </div>
            </div>

            <div class="col">
                <div class="form-group">
                    <label>Fotos del comercio:</label>
                    <input type="file" class="form-control-file">
                </div>

                <div class="form-group">
                    <label>Horario de atencion:</label>
                </div>

                <div class="form-group">
                    <label>Redes sociales:</label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Direccion:</label>
                    <input type="text" class="form-control">
                </div>

                <div class="form-group">
                    <div id="mapAdminComercio">
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="form-group">
                    <label>Promociones:</label>
                </div>

                <div class="form-group">
                    <label>Articulos:</label>
                </div>
            </div>
        </div>
        <div class="row align-content-center">
            <div class="col-md-4">
            </div>
            <div class="col-md-2">
                <button class="btn btn-primary">Registrar</button>
            </div>
            <div class="col-md-2">
                <button class="btn btn-danger">Cancelar</button>
            </div>
            <div class="col-md-4">
            </div>
        </div>
    </div>
</asp:Content>
