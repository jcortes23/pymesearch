﻿var APP = window.APP || {}

APP.AdministrarComercio = function () {

    var init = function () {
        console.log('RegistroComercio');
        inicializarCoordenadas();
    }

    var inicializarCoordenadas = function () {
        var mapAdminComercio = L.map('mapAdminComercio', { center: [9.98093, -84.24184], zoom: 15, maxZoom: 18, minZoom: 3 })
        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>'
        }).addTo(mapAdminComercio);
        L.control.scale().addTo(mapAdminComercio);

        var markerMiCasa = L.marker([9.98093, -84.24184], { draggable: false }).addTo(mapAdminComercio);
        markerMiCasa.bindPopup("<b>Casa del Kano</b><br>Aqui esta mi casa");

        var markerCasaGrone = L.marker([9.97476, -84.23924], { draggable: false }).addTo(mapAdminComercio);
        markerCasaGrone.bindPopup("<b>Casa del Grone</b><br>Aqui esta la casa del grone pendejo");

        var popup = L.popup().setLatLng([9.9867, -84.2435]).setContent("Este es un popup solo").openOn(mapAdminComercio);
        var lastMarker;

        //function onMapClick(e) {
        //    if (lastMarker !== undefined) {
        //        map.removeLayer(lastMarker);
        //    }
        //    var marker = L.marker([e.latlng.lat, e.latlng.lng], { draggable: false }).addTo(map);
        //    marker.bindPopup("Latitud: " + e.latlng.lat.toString() + "<br>Longitud: " + e.latlng.lng.toString());
        //    lastMarker = marker;
        //}
        //map.on('click', onMapClick);
    }

    return {
        init: init
    }
}();
$(document).ready(function () {
    APP.AdministrarComercio.init();
});