﻿var APP = window.APP || {}

APP.Principal = function () {

    var init = function () {
        console.log('Principal');
        inicializarComponentes();
    }
    var inicializarCoordenadas = function (lat, lon) {
        var map = L.map('map', { center: [lat, lon], zoom: 15, maxZoom: 18, minZoom: 3 })
        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>'
        }).addTo(map);
        L.control.scale().addTo(map);       

        var UbicacionActual = L.marker([lat, lon], { draggable: false }).addTo(map);
        UbicacionActual.bindPopup("<b>Estas aquí</b><br>");

        //var markerCasaGrone = L.marker([9.97476, -84.23924], { draggable: false }).addTo(map);
        //markerCasaGrone.bindPopup("<b>Casa del Grone</b><br>Aqui esta la casa del grone pendejo");

        //var popup = L.popup().setLatLng([9.9867, -84.2435]).setContent("Estás aquí").openOn(map);
        //var lastMarker;

        //function onMapClick(e) {
        //    if (lastMarker !== undefined) {
        //        map.removeLayer(lastMarker);
        //    }
        //    var marker = L.marker([e.latlng.lat, e.latlng.lng], { draggable: false }).addTo(map);
        //    marker.bindPopup("Latitud: " + e.latlng.lat.toString() + "<br>Longitud: " + e.latlng.lng.toString());
        //    lastMarker = marker;
        //}
        //map.on('click', onMapClick);
        //marker.bindPopup("<b>Hello world!</b><br>I am a popup.").openPopup();
        //var popup = L.popup()
        //    .setLatLng([lat, lon])
        //    .setContent("I am a standalone popup.")
        //    .openOn(map);
    }
    var inicializarComponentes = function () {

        //inicializarCoordenadas();
        obtenerGeolocalizacion();
    }
    var obtenerGeolocalizacion = function () {


        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (objPosition) {
                var lon = objPosition.coords.longitude;
                var lat = objPosition.coords.latitude;
                inicializarCoordenadas(lat, lon);
                //content.innerHTML = "<p><strong>Latitud:</strong> " + lat + "</p><p><strong>Longitud:</strong> " + lon + "</p>";

            }, function (objPositionError) {
                switch (objPositionError.code) {
                    case objPositionError.PERMISSION_DENIED:
                        //content.innerHTML = "No se ha permitido el acceso a la posición del usuario.";
                        break;
                    case objPositionError.POSITION_UNAVAILABLE:
                        // content.innerHTML = "No se ha podido acceder a la información de su posición.";
                        break;
                    case objPositionError.TIMEOUT:
                        //content.innerHTML = "El servicio ha tardado demasiado tiempo en responder.";
                        break;
                    default:
                    //content.innerHTML = "Error desconocido.";
                }
            }, {
                maximumAge: 75000,
                timeout: 15000
            });
        }
        else {
            //content.innerHTML = "Su navegador no soporta la API de geolocalización.";
        }
    }
    return {
        init: init
    }
}();
$(document).ready(function () {
    APP.Principal.init();
});